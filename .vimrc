" Small vim-config file with options and plugins for web development
" @copyright 2021
" @package by Denis N.

let data_dir = '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
    if !executable("curl")
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
    endif
    echo "Installing Vim-Plug..."
    echo ""
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


"plugins
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' } "Плагин предназначен для работы с файловой системой.
Plug 'morhetz/gruvbox' "Плагин цветовой схемы gruvbox
Plug 'vim-airline/vim-airline' "Плагин отображает удобную строку статуса
Plug 'vim-airline/vim-airline-themes'
Plug 'hail2u/vim-css3-syntax', { 'for': ['css', 'scss'] } "css syntax
Plug 'Raimondi/delimitMate' "Плагин обеспечивает автоматическое закрытие кавычек, парных тегов, скобок
Plug 'mattn/emmet-vim', { 'for': ['html', 'php', 'htm', 'tpl'] } "Плагин автодополнения тегов html по Ctrl + y + ,
Plug 'jelera/vim-javascript-syntax', { 'for': 'javascript' } "Плагин для поддержки синтаксиса JS
"Plug 'shawncplus/phpcomplete.vim', { 'for': 'php'} "Плагин автодополнения и доков php
Plug 'yggdroot/indentline', { 'for': ['html', 'php', 'htm', 'tpl', 'javascript', 'typescript', 'typescriptreact'] } "Плагин добавляет вертикальные линии отступов
Plug 'junegunn/fzf', { 'do': './install --all' } | Plug 'junegunn/fzf.vim' "Плагин нечеткого поиска fzf
Plug 'godlygeek/tabular' "Плагин для расстановки табов по коду
Plug 'valloric/matchtagalways', has('python') ? { 'for': ['html', 'xml', 'php', 'tpl'] } : { 'on': [] } "Плагин всегда показывает парные теги xml/html
Plug 'tomtom/tcomment_vim' "Плагин комментирования строк кода
Plug 'conradirwin/vim-bracketed-paste' "Плагин автоматической установки :set paste при вставке текста
Plug 'Shadowsith/vim-minify', { 'for': ['css', 'scss', 'javascript', 'html', 'php'] } "Плагин минификации js и css
Plug 'othree/eregex.vim' " Поиск и замена текста в формате Perl
"v8+
Plug 'tpope/vim-dispatch', v:version>=800 ? {} : { 'on': [] } " Асинхронный диспетчер. Для обновления vimrc
Plug 'prabirshrestha/asyncomplete.vim', (v:version>=800 && executable('npm')) ? {} : { 'on': [] } " Автодополнения в vim
Plug 'prabirshrestha/vim-lsp', (v:version>=800 && executable('npm')) ? {} : { 'on': [] } " Асинхронный Language Server Protocol
Plug 'mattn/vim-lsp-settings', (v:version>=800 && executable('npm')) ? {} : { 'on': [] } " Автоматическая установка языка для vim-lsp
Plug 'prabirshrestha/asyncomplete-lsp.vim', ( v:version>=800 && executable('npm')) ? {} : { 'on': [] } " Автодополнения для lsp
Plug 'ludovicchabant/vim-gutentags', (v:version>=800 && executable('ctags')) ? {} : { 'on': [] } " Сборщик тегов для перехода к определениям методов и классов 
Plug 'herringtondarkholme/yats.vim', { 'for': ['ts', 'tsx'] } " Подсветка кода TypeScript
" Plug 'neoclide/coc.nvim', (v:version>=820) ? {'branch': 'release', 'for': ['ts', 'tsx', 'typescript', 'typescriptreact'] } : { 'on' : [] } " Плагин для react
Plug 'airblade/vim-gitgutter' " Показываем изменения в Git

call plug#end()


"*******************"
"" Базовые настройки 
"*******************"
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set smartindent
set nocompatible
set nrformats=

"" Табуляция вместо пробелов (Bitrix)
"set noexpandtab
"" Пробелы вместо табуляции (PSR)
set expandtab

"" Свертка кода
set foldmethod=indent
set nofoldenable
set foldlevel=99

"" Поиск 
set incsearch
set hlsearch
set ignorecase
set smartcase
" Search mappings: Показать найденное вхождение в центре экрана
nnoremap n nzzzv
nnoremap N Nzzzv

"" Кодировка 
set modifiable
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8,cp1251,koi8-r,ucs-2,cp866
set ttyfast

set fileformats=unix,dos,mac

"" Отключаем звуки
set noerrorbells
set t_vb=
"" Отключаем мигающий курсор
set novisualbell

"" Отключаем .swp и бэкап-файлы
set nobackup
set noswapfile
" Директория для сохранения всех файлов подкачки в одном месте
"set directory=$HOME/.vim/swp//

"" Соханение изменений в буфере без записи на диск
set hidden

" Восстановлении последней позиции в файле
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Отображать диалоговое окно подтверждения при закрытии несохраненного файла
set confirm

" Устанавливаем время задержки ввода комбинаций 
set tm=500

" Освобождаем Backspace
set backspace=start,indent,eol

" Включает определение типа файла, загрузку соответствующих ему плагинов и файлов отступов
filetype plugin indent on

" Уменьшаем время обновления для уведомлений
set updatetime=300

" Устраняем уязвимость выполнения произвольного кода в файле в старых версиях
" Vim/Neovim Arbitrary Code Execution via Modelines
if v:version < 800
    set nomodeline
endif

"**********************"
"" Визуальные настройки 
"**********************"
syntax on
set ruler

" Нумерация строк
if exists('+relativenumber')
    set rnu " относительная нумерация
    set nu " обычная нумерация
else
    set nu " обычная нумерация
end

" Цветовая схема
silent! colorscheme gruvbox
set background=dark

" Установить заголовок окна, отражающий редактируемый в данный момент файл
set title
set titleold="Terminal"
set titlestring=%F

" Переносим на другую строчку, разрываем строки
set wrap
" Перенос строк в удобных точках, избегать переноса строки в середине слова
set linebreak

" Включаем 256 цветов в терминале, мы ведь работаем из иксов?
" Нужно во многих терминалах, например в gnome-terminal
set t_Co=256

" Настройки размера окна терминала
if v:version >= 800
    set termwinsize=5x0
endif

" Не снимать выделение после табуляции
vnoremap < <gv
vnoremap > >gv

" Отключаем подсветку синтаксиса на больших файлах
autocmd BufWinEnter * if line2byte(line("$") + 1) > 10000000 | syntax clear | endif

" Включаем умные отступы, т.к. сами они не включаются
autocmd VimEnter * set smartindent

" Выделение цветом ограничения на длину строки
if exists('+colorcolumn')
    set colorcolumn=80 " Подсвечиваем строки длиннее 80 символов
end

" Отступы для css/html/typescript-файлов
autocmd FileType css,scss,sass,html,typescript,typescriptreact setlocal shiftwidth=2 softtabstop=2 expandtab

"***************************************"
"" Netrw настройки файлового менеджера ""
"***************************************"
let g:netrw_banner = 0 "Скрыть баннер над файлами
let g:netrw_liststyle = 3 "Дерево вместо плоского списка
let g:netrw_browse_split = 2 "Открывать файлы в новой вкладке
let g:netrw_winsize = 25 "Размер окна netrw
set autochdir "изменить директорию на директорию текущего буфера


"********************"
"" Настройки плагинов
"********************"

" nerdtree
" Автозапуск NERDTree. Если открывается файл, фокус ввода перемещается в него
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
" Выход из VIM, когда закрыты все окна и осталось только окно NERDTree.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Переходим в директорию с открытым файлом
autocmd BufEnter * silent! lcd %:p:h
" Показ скрытых файлов
let NERDTreeShowHidden=1

" vim-airline
let g:airline_theme = 'powerlineish'
let g:airline#extensions#branch#enabled = 1 
let g:airline#extensions#ale#enabled = 1 
let g:airline#extensions#tabline#enabled = 1 
let g:airline#extensions#tagbar#enabled = 1 
let g:airline_skip_empty_sections = 1 
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
    let g:airline#extensions#tabline#left_sep = ' '
    let g:airline#extensions#tabline#left_alt_sep = '|'
    let g:airline_left_sep          = '▶'
    let g:airline_left_alt_sep      = '»'
    let g:airline_right_sep         = '◀'
    let g:airline_right_alt_sep     = '«'
    let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
    let g:airline#extensions#readonly#symbol   = '⊘'
    let g:airline#extensions#linecolumn#prefix = '¶'
    let g:airline#extensions#paste#symbol      = 'ρ'
    let g:airline_symbols.linenr    = '␊'
    let g:airline_symbols.branch    = '⎇'
    let g:airline_symbols.paste     = 'ρ'
    let g:airline_symbols.paste     = 'Þ'
    let g:airline_symbols.paste     = '∥'
    let g:airline_symbols.whitespace = 'Ξ'
else
    let g:airline#extensions#tabline#left_sep = ''
    let g:airline#extensions#tabline#left_alt_sep = ''

    " powerline symbols
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ''
endif

" indentline
"let g:indentLine_setColors = 0
let g:indentLine_color_term = 239
let g:vim_json_conceal=0 "disable for json
let g:markdown_syntax_conceal=0 "disable for markdown

" matchtagalways
if(has('python'))
    let g:mta_use_matchparen_group = 1
    let g:mta_filetypes = {'html' : 1, 'xhtml' : 1, 'xml' : 1, 'php' : 1}
endif

"delimitMate
"" включить отступы по нажатию на пробел. " |" превращается в " | "
let delimitMate_expand_space = 1

" vim-lsp
let g:lsp_diagnostics_enabled = 0

" gutentags
let g:gutentags_project_root = ['~/', '.git', 'app']
let g:gutentags_ctags_exclude = [
      \ '*.git', '*.svg', '*.hg',
      \ '*/tests/*',
      \ 'build',
      \ 'dist',
      \ '*sites/*/files/*',
      \ 'upload/*',
      \ 'node_modules',
      \ 'bower_components',
      \ 'cache',
      \ 'compiled',
      \ 'docs',
      \ 'example',
      \ 'bundle',
      \ 'vendor',
      \ '*.md',
      \ '*-lock.json',
      \ '*.lock',
      \ '*.js',
      \ '.*rc*',
      \ '*.json',
      \ '*.min.*',
      \ '*.map',
      \ '*.bak',
      \ '*.zip',
      \ '*.pyc',
      \ '*.class',
      \ '*.sln',
      \ '*.Master',
      \ '*.csproj',
      \ '*.tmp',
      \ '*.cache',
      \ 'tags*',
      \ '*.css',
      \ '*.less',
      \ '*.scss',
      \ '*.swp', '*.swo',
      \ '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png',
      \ '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
      \ '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx',
      \ ]

" eregex
let g:eregex_default_enable = 0

"***************************"
"" COC NVIM
"***************************"

let g:coc_disable_startup_warning = 1

" GoTo code navigation
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)

augroup CocGroup
  autocmd!
  " Setup formatexpr specified filetype(s)
  autocmd FileType typescript,typescriptreact,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Настройки для vim-gitgutter
set updatetime=100

"***************************"
"" Маппинг комбинаций клавиш
"***************************"

" маппинг leader key
let g:mapleader = ","

map <C-n> :NERDTreeToggle<CR>
map <C-j> :IndentLinesToggle<CR>
map <C-/> :Tcomment<CR>

" Подставляем закрывающий тег для <? в PHP
function! InsertPhpTag()
    if !empty(getline("."))
        return "<??>\<Esc>hi"
    else
        return "<?"
    endif
endfunction
au FileType php inoremap <? <C-R>=InsertPhpTag()<CR>

" Omni-дополнение
imap <C-F> <C-X><C-O>

" Перемещение строк
" nnoremap <S-j> :m .+1<CR>==
" nnoremap <S-k> :m .-2<CR>==
" vnoremap <S-j> :m '>+1<CR>gv=gv
" vnoremap <S-k> :m '<-2<CR>gv=gv

" Нечеткий fzf-поиск по файлам
nmap <C-P> :Files ~<CR>

" Ctrl+C как Esc
imap <C-c> <Esc>

" asyncomplete. Вставка дополнений по Tab + Enter
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

" Переключение отображения нумерации строк
nnoremap <leader>nn :set nu! rnu!<CR>

" Переключение отображения изменений vim-gutter
nnoremap <leader>gg :call ToggleGitGutter()<CR>

" Переключение отображения изменений vim-gutter
nnoremap <leader>t :bo term<CR>


"*******************"
"" Кастомные функции
"*******************"

" Переключение vim-gutter
function! ToggleGitGutter()
  if exists('g:gitgutter_enabled') && g:gitgutter_enabled
    GitGutterDisable
  else
    GitGutterEnable
  endif
endfunction


" Автообновление vimrc при запуске
if v:version >= 800
	let git_dir = '~/.vim/vimrc'
	if !empty(glob(git_dir . '/.git'))
        if has_key(plugs, 'vim-dispatch')
            autocmd VimEnter * Dispatch! git -C ~/.vim/vimrc reset --hard
            autocmd VimEnter * Dispatch! git -C ~/.vim/vimrc pull
            autocmd VimEnter * Dispatch! cp ~/.vim/vimrc/.vimrc ~/
        endif
	endif
endif

" Вставить <Tab> или дополнить идентификатор,
" если курсор находится после символа ключевого слова
function! InsertTabOrComplete()
    let col = col('.') - 1 
    if !col || getline('.')[col-1] !~ '\k'
        return "\<tab>"
    else
        return "\<C-N>"
    endif
endfunction

inoremap <Tab> <C-R>=InsertTabOrComplete()<CR>

" Функция для ручного обновления vimrc
function! VimrcRemoteUpdate()
	let git_dir = '~/.vim/vimrc'
    if(executable('git'))
        if !empty(glob(git_dir . '/.git'))
            " new git version
            "silent execute '!git -C ~/.vim/vimrc reset --hard'
            "silent execute '!git -C ~/.vim/vimrc pull'
            " old git version
            silent execute '!git --git-dir ~/.vim/vimrc/.git --work-tree ~/.vim/vimrc reset --hard'
            silent execute '!git --git-dir ~/.vim/vimrc/.git --work-tree ~/.vim/vimrc pull'
            " copy fresh file
            silent execute '!cp ~/.vim/vimrc/.vimrc ~/'
            silent PlugInstall
            silent PlugClean!
        else
            silent execute '!mkdir -p ~/.vim && cd ~/.vim && git clone https://nabatchikov@gitlab.com/nabatchikov/vimrc.git'
            :call VimrcRemoteUpdate()
        endif
    endif
endfunction

:command! -nargs=0 UpdateVimrc :call VimrcRemoteUpdate()

